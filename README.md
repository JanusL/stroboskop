# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://JanusL@bitbucket.org/JanusL/stroboskop.git
cd stroboskop
```

Naloga 6.2.3:
https://bitbucket.org/JanusL/stroboskop/commits/1607abf96465cd11aaba8067aef6d1e9b927536f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/JanusL/stroboskop/commits/4565f29e67a899171ca7fad6fe39e1c1c3227a6e

Naloga 6.3.2:
https://bitbucket.org/JanusL/stroboskop/commits/255b14514759a1bbb462b8a23d99da057041a50d

Naloga 6.3.3:
https://bitbucket.org/JanusL/stroboskop/commits/6ffa8c7919a75734943b278edff800506a289564

Naloga 6.3.4:
https://bitbucket.org/JanusL/stroboskop/commits/fa3621bc9068bb0c17fcead8d9aca1a75127561f  //uveljavitev na gumbih
https://bitbucket.org/JanusL/stroboskop/commits/f410209c3c64fa8842700c70d78d91f92550e53b  //uveljavitev na telesu
https://bitbucket.org/JanusL/stroboskop/commits/4828398d77748ea0a18da59c000bb1fcd34aef3d  // še na gumbu za stroboskop, opa ¯\_(ツ)_/¯

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/JanusL/stroboskop/commits/32c41b77fb6fa74d6d78c72e1fd6fd2c0f8b27c3

Naloga 6.4.2:
https://bitbucket.org/JanusL/stroboskop/commits/fa25e99bc426354f67fe0326f410acff4bdacef3

Naloga 6.4.3:
https://bitbucket.org/JanusL/stroboskop/commits/43ac9e96d077e5f02f989b6329392c35c4b9cffd

Naloga 6.4.4:
https://bitbucket.org/JanusL/stroboskop/commits/05d8226b3d449bae20014aa431ec00b609aee8f8